# IPLM: An IP List Manager

iplm is an open-source web IP address management application (IPAM). Its goal is to provide light, modern and useful IP address management.

The application is build using the [Ionic Framework](http://ionicframework.com) and [Angular2](https://angular.io).  The database is using [Firebase](https://firebase.google.com/)
