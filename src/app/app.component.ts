import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { AccountPage } from '../pages/account/account';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SettingsPage } from '../pages/settings/settings';
import { AuthService } from '../providers/auth-service';
import { StateService } from '../providers/state-service'

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  pages: Array<{ title: string, component: any, icon: string }>;

  constructor(public platform: Platform, public auth: AuthService, public state$: StateService) {
    this.initializeApp();

    this.auth.auth.subscribe(data => {
      if (data != null) {
        // The user is logged in and the JWT has not expired
        // just send them to the HomePage
        this.rootPage = HomePage;
      }
      else {
        this.rootPage = LoginPage
      }
    });

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, icon: 'home' },
      { title: 'Settings', component: SettingsPage, icon: 'settings' },
      { title: 'Account', component: AccountPage, icon: 'contact' }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  signOut() {
    this.rootPage = LoginPage;
    this.state$.removeState();
    this.auth.signOut();
  }
}
