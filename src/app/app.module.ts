import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';
import { MyApp } from './app.component';

import { AccountPage } from '../pages/account/account';
import { DeviceDetailsPage } from '../pages/device-details/device-details';
import { DetailsListPage } from '../pages/details-list/details-list';
import { GroupDetailsPage } from '../pages/group-details/group-details';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SearchPage } from '../pages/search/search';
import { SettingsPage } from '../pages/settings/settings';

import { AuthService } from '../providers/auth-service';
import { DeviceService } from '../providers/device-service';
import { GroupService } from '../providers/group-service';
import { SearchService } from '../providers/search-service';
import { StateService } from '../providers/state-service';
import { UserProfileService } from '../providers/user-profile-service';

// Must export the config
export const firebaseConfig = {
  apiKey: '',
  authDomain: '',
  databaseURL: '',
  storageBucket: ''
};

const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
};


@NgModule({
  declarations: [
    MyApp,
    AccountPage,
    DeviceDetailsPage,
    DetailsListPage,
    GroupDetailsPage,
    HomePage,
    LoginPage,
    SearchPage,
    SettingsPage
  ],
  imports: [
    AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AccountPage,
    DeviceDetailsPage,
    DetailsListPage,
    GroupDetailsPage,
    HomePage,
    LoginPage,
    SearchPage,
    SettingsPage
  ],
  providers: [
    AuthService,
    DeviceService,
    GroupService,
    SearchService,
    StateService,
    UserProfileService,
    {
      provide: ErrorHandler,
      useClass: IonicErrorHandler
    }
  ]
})
export class AppModule { }
