export interface IDevice {
    $key: string;
    location: string;
    pc_name?: string;
    notes?: string;
    device: string;
    description: string;
    ip_address: string;
    mac_address: string;
    group_key: string;
    plc_type?: string;
    firmware?: string;
    port_type?: string;
    port?: number;
}

export class Device implements IDevice {
    $key: string;
    description: string;
    location: string;
    pc_name?: string;
    notes?: string;
    device: string;
    ip_address: string;
    mac_address: string;
    group_key: string;
    plc_type?: string;
    firmware?: string;
    port_type?: string;
    port?: number;

    constructor(
        description: string,
        location: string,
        device: string,
        ip_address: string,
        group_key: string,
        pc_name?: string,
        notes?: string,
        mac_address?: string,
        plc_type?: string,
        firmware?: string,
        port_type?: string,
        port?: number) {
        this.description = description == null ? '' : description;
        this.location = location;
        this.pc_name = pc_name;
        this.notes = notes;
        this.device = device;
        this.ip_address = ip_address;
        this.group_key = group_key;
        this.mac_address = mac_address;
        this.plc_type = plc_type;
        this.firmware = firmware;
        this.port_type = port_type;
        this.port = port;
    }
}
