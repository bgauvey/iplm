export interface IipInfo {
    range: string;
    subnet: string;
    gateway: string;
    dns: string;
    dns2: string;
    static_range?: string;
}

export interface IGroup {
    $key?: string;
    name: string;
    notes?: string;
    ip_info: IipInfo;
}

export class Group implements IGroup {
    name: string;
    notes?: string;
    ip_info: IipInfo;

    constructor(
        name: string,
        notes: string,
        ip_info: IipInfo
    ) {
        this.name = name;
        this.notes = notes;
        this.ip_info = ip_info;
    }
}

export class IpInfo implements IipInfo {
    range: string;
    subnet: string;
    gateway: string;
    dns: string;
    dns2: string;
    static_range?: string;

    constructor(
        range: string,
        subnet: string,
        gateway: string,
        dns: string,
        dns2: string,
        static_range: string
    ) {
        this.range = range;
        this.subnet = subnet;
        this.gateway = gateway;
        this.dns = dns;
        this.dns2 = dns2;
        this.static_range = static_range;
    }
}
