import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from 'ionic-angular';

import { HomePage } from '../home/home';
import { AuthService } from '../../providers/auth-service';
import { UserProfileService } from '../../providers/user-profile-service';

function passwordMatcher(c: AbstractControl) {
  return c.get('password').value === c.get('confirm').value
    ? null : { 'nomatch': true };
}
/*
  Generated class for the Account page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {
  form: FormGroup;
  error: string;
  private key: string;
  private profile: any;

  constructor(public navCtrl: NavController,
    public fb: FormBuilder,
    public auth: AuthService,
    private user$: UserProfileService, ) {
    this.form = this.fb.group({
      email: [{ value: '', disabled: true }, Validators.required],
      first_name: '',
      last_name: '',
      password: ['', Validators.required],
      confirm: ['', Validators.required]
    }, { validator: passwordMatcher });
  }

  ionViewDidLoad() {
    this.auth.userInfo().subscribe(
      userInfo => {
        this.profile = {
          first_name: userInfo.first_name,
          last_name: userInfo.last_name,
          isAdmin: userInfo.isAdmin,
          email: userInfo.email
        };
        this.key = userInfo.$key;
        this.form.setValue(
          {
            email: userInfo.email,
            first_name: userInfo.first_name,
            last_name: userInfo.last_name,
            password: '',
            confirm: ''
          });
      });
  }
  cancel(e) {
    e.preventDefault();
    this.navCtrl.setRoot(HomePage);
  }

  onSubmit(form: any, valid: boolean) {
    // this.profile.first_name = form.first_name;
    // this.profile.last_name = form.last_name;
    // this.user$.update(this.key, this.profile);

    if (this.form.controls['password'].dirty) {
      // change password
      this.auth.changePassword(form.password)
        .then(res => {
          console.info(res);
        })
        .catch(err => {
          this.error = err.message;
          console.error(err);
        });
    }
  }
}
