import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DeviceService } from '../../providers/device-service';
import { GroupService } from '../../providers/group-service'
import { StateService } from '../../providers/state-service'
import { DeviceDetailsPage } from '../device-details/device-details';
import { SearchPage } from '../search/search'

@Component({
  selector: 'page-details-list',
  templateUrl: 'details-list.html'
})
export class DetailsListPage {
  title: string;
  devices: any;
  isAdmin: boolean = false;
  private _groupKey: string;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public group$: GroupService,
    public device$: DeviceService,
    public state$: StateService) {
    this._groupKey = navParams.get('key');
  }

  ionViewDidLoad() {
    this.state$.getState().subscribe(state => {
      if (state != null) {
        this.isAdmin = state.user == null ? false : state.user.isAdmin;
      }
    });
    this.group$
      .getById(this._groupKey)
      .subscribe(data => {
        this.title = data.name;
      });

    this.device$.getByParentKey(this._groupKey).subscribe(data => { this.devices = data });
  }

  showDetails(key: string) {
    this.navCtrl.push(DeviceDetailsPage, { key: key })
  }

  showSearch() {
    this.navCtrl.push(SearchPage);
  }

  getClass(deviceName: any) {
    return 'icon-' + deviceName[0].toLowerCase();
  }

  createDevice() {
    this.navCtrl.push(DeviceDetailsPage, { key: 'new~' + this._groupKey })
  }

  doRefresh(refresher: any) {
    this.device$.getByParentKey(this._groupKey).subscribe(data => {
      this.devices = data;
      refresher.complete();
    });
  }
}
