import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { Dialogs } from 'ionic-native';

import { DeviceService } from '../../providers/device-service';
import { StateService } from '../../providers/state-service'
import { Device } from '../../models/device-model';

@Component({
  selector: 'page-device-details',
  templateUrl: 'device-details.html'
})
export class DeviceDetailsPage {
  title: string = 'Edit Details';
  deviceObj: Device;
  form: FormGroup;
  data: any;
  groupKey: string;
  isAdmin: boolean;
  public submitted: boolean; // keep track on whether form is submitted

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public toastCtrl: ToastController,
    private fb: FormBuilder,
    private device$: DeviceService,
    public state$: StateService) {
    this.data = [
      { value: 'Device', label: 'Device' },
      { value: 'HMI', label: 'HMI' },
      { value: 'PC', label: 'PC' },
      { value: 'PLC', label: 'PLC' },
      { value: 'Printer', label: 'Printer' },
      { value: 'Switch', label: 'Switch' }
    ];

    this.state$.getState().subscribe(state => {
      if (state != null) {
        this.isAdmin = state.user == null ? false : state.user.isAdmin;
      }
    });

    this.form = this.fb.group({
      description: ['', [Validators.required]],
      location: ['', [Validators.required]],
      ip_address: ['', Validators.required],
      device: ['', Validators.required],
      notes: [''],
      pc_name: [''],
      mac_address: [''],
      group_key: [''],
      firmware: [''],
      plc_type: [''],
      port_type: [''],
      port: [''],
      $key: ['']
    });
  }

  ionViewCanLeave(): Promise<{}> {
    if (!this.form.pristine && !this.submitted) {
      return new Promise((resolve, reject) => {
        Dialogs.confirm('Your changes have not been saved. Do you want to discard changes?', 'Discard Changes?', ['NO', 'YES'])
          .then(value => {
            if (value === 2) {
              resolve();
            } else {
              reject();
            }
          });
      });
    } else {
      return Promise.resolve({});
    }
  }

  ionViewDidLoad() {
    let key: string;
    key = this.navParams.get('key');
    if (key.startsWith('new')) {
      this.title = 'New Device';
      this.deviceObj = {
        $key: null, mac_address: null, notes: null, description: null,
        location: null, device: null, ip_address: null, group_key: key.split('~')[1]
      };
      this.groupKey = this.deviceObj.group_key;
    } else {
      this.device$.getById(key).subscribe(o => {
        this.deviceObj = new Device(o.description, o.location, o.device, o.ip_address,
          o.group_key, o.pc_name == null ? '' : o.pc_name, o.notes == null ? '' : o.notes,
          o.mac_address == null ? '' : o.mac_address, o.plc_type == null ? '' : o.plc_type,
          o.firmware == null ? '' : o.firmware, o.port_type == null ? '' : o.port_type,
          o.port == null ? 0 : o.port);
        this.deviceObj.$key = o.$key;
        this.form.setValue(this.deviceObj);
      });
    }
  }


  save(model: Device, valid: boolean) {
    this.submitted = true; // set form submit to true

    // check if model is valid
    // if valid, call API to save device
    if (model != null) {
      if (this.deviceObj.$key == null) {
        console.log(model);
        this.device$.create({
          device: model.device,
          group_key: this.groupKey,
          description: model.description,
          ip_address: model.ip_address,
          location: model.location,
          mac_address: model.mac_address,
          notes: model.notes,
          pc_name: model.pc_name,
          plc_type: model.plc_type,
          firmware: model.firmware,
          port_type: model.port_type,
          port: model.port
        })
          .then(data => {
            let toast = this.toastCtrl.create({ message: 'Created successfully', duration: 4000 });
            toast.present();
            this.navCtrl.pop();
          })
          .catch(err => {
            let toast = this.toastCtrl.create({ message: err.message, duration: 4000 });
            toast.present();
          });
      } else {
        this.device$
          .update(model.$key, {
            device: model.device,
            group_key: model.group_key,
            ip_address: model.ip_address,
            description: model.description,
            location: model.location,
            mac_address: model.mac_address,
            notes: model.notes,
            pc_name: model.pc_name,
            plc_type: model.plc_type,
            firmware: model.firmware,
            port_type: model.port_type,
            port: model.port
          })
          .then(data => {
            let toast = this.toastCtrl.create({ message: 'Updated successfully', duration: 4000 });
            toast.present();
          })
          .catch(err => {
            let toast = this.toastCtrl.create({ message: err.message, duration: 4000 });
            toast.present();
          });
      }
    }
  }

  delete() {
    Dialogs.confirm('Do you want to delete this device?', 'Confirm Delete', ['CANCEL', 'DELETE'])
      .then(value => {
        if (value === 2) {
          this.device$
            .remove(this.deviceObj.$key)
            .then(data => {
              let toast = this.toastCtrl.create({ message: 'The item was successfully removed.', duration: 2000 });
              toast.present();
              this.navCtrl.pop();
            })
            .catch(err => {
              let toast = this.toastCtrl.create({ message: err.message, duration: 4000 });
              toast.present();
            });
        }
      });
  }
}
