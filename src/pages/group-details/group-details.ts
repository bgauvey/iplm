import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { Dialogs } from 'ionic-native';

import { GroupService } from '../../providers/group-service'
import { Group, IpInfo } from '../../models/group-model';

@Component({
  selector: 'page-group-details',
  templateUrl: 'group-details.html'
})
export class GroupDetailsPage {
  title: string = 'Edit Details';
  group: any;
  form: FormGroup;
  public submitted: boolean; // keep track on whether form is submitted

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    private fb: FormBuilder,
    private group$: GroupService) {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      notes: '',
      ip_info: this.fb.group({
        range: '',
        subnet: '',
        gateway: '',
        dns: '',
        dns2: '',
        static_range: ''
      }),
      $key: ''
    });
  }

  ionViewCanLeave(): Promise<{}> {
    if (!this.form.pristine && !this.submitted) {
      return new Promise((resolve, reject) => {
        Dialogs.confirm('Your changes have not been saved. Do you want to discard changes?', 'Discard Changes?', ['NO', 'YES'])
          .then(value => {
            if (value === 2) {
              resolve();
            } else {
              reject();
            }
          });
      });
    } else {
      return Promise.resolve({});
    }
  }

  ionViewDidLoad() {
    let key: string = this.navParams.get('key');

    if (key != null) {
      this.group$.getById(key).subscribe(
        data => {
          this.group = {
            name: data.name,
            notes: data.notes,
            ip_info: {
              range: data.ip_info.range,
              subnet: data.ip_info.subnet,
              gateway: data.ip_info.gateway,
              dns: data.ip_info.dns,
              dns2: data.ip_info.dns2,
              static_range: data.ip_info.static_range,
            },
            $key: data.$key
          }
        });
      this.form.setValue(this.group);
    } else {
      this.title = 'New Folder';
    }
  }

  save(model: Group, valid: boolean) {
    this.submitted = true; // set form submit to true

    // check if model is valid
    // if valid, call API to save group
    if (valid) {
      if (this.title == 'New Folder') {
        this.group$
          .create(new Group(
            model.name,
            model.notes,
            new IpInfo(
              model.ip_info.range,
              model.ip_info.subnet,
              model.ip_info.gateway,
              model.ip_info.dns,
              model.ip_info.dns2,
              model.ip_info.static_range
            )
          ))
          .then(data => {
            let toast = this.toastCtrl.create({ message: 'Created successfully', duration: 4000 });
            toast.present();
          })
          .catch(err => {
            let toast = this.toastCtrl.create({ message: err.message, duration: 4000 });
            toast.present();
          });
      } else {
        this.group$
          .update(this.group, new Group(
            model.name,
            model.notes,
            new IpInfo(
              model.ip_info.range,
              model.ip_info.subnet,
              model.ip_info.gateway,
              model.ip_info.dns,
              model.ip_info.dns2,
              model.ip_info.static_range
            )
          ))
          .then(data => {
            let toast = this.toastCtrl.create({ message: 'Updated successfully', duration: 4000 });
            toast.present();
          })
          .catch(err => {
            let toast = this.toastCtrl.create({ message: err.message, duration: 4000 });
            toast.present();
          });
      }
    }
  }
}
