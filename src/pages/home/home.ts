import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from 'ionic-angular';

import { GroupService } from '../../providers/group-service'
import { StateService } from '../../providers/state-service'

import { DetailsListPage } from '../details-list/details-list'
import { GroupDetailsPage } from '../group-details/group-details';
import { SearchPage } from '../search/search'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  list: any;
  isAdmin: boolean = false;
  constructor(public navCtrl: NavController,
    public group$: GroupService,
    public state$: StateService,
    public loadingCtrl: LoadingController) { }

  ngOnInit() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.group$.get()
      .subscribe(data => {
        this.list = data;
        loading.dismiss();
      }, err => {
        console.error(err);
        loading.dismiss();
      });
  }

  ionViewDidLoad() {
    this.state$.getState().subscribe(state => {
      if (state != null) {
        this.isAdmin = state.user == null ? false : state.user.isAdmin;
      }
    });
  }

  showDetails(group: any) {
    this.navCtrl.push(GroupDetailsPage, { 'key': group.$key });
  }

  showDevices(group: any) {
    this.navCtrl.push(DetailsListPage, { 'key': group.$key });
  }

  showSearch() {
    this.navCtrl.push(SearchPage);
  }

  createGroup() {
    this.navCtrl.push(GroupDetailsPage);
  }

  doRefresh(refresher: any) {
    this.group$.get().subscribe(data => {
      this.list = data;
      refresher.complete();
    });
  }
}
