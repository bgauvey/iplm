import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { AuthService } from '../../providers/auth-service';
import { UserProfileService } from '../../providers/user-profile-service';
import { StateService } from '../../providers/state-service';

import { HomePage } from '../home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  errorMessage: string;
  showForgotPassword: boolean = false;
  showVerification: boolean = false;
  email: string;

  private _authenticated = false;
  constructor(public navCtrl: NavController,
    public auth: AuthService,
    public state$: StateService,
    public user$: UserProfileService,
    public toastCtrl: ToastController) { }

  ionViewDidLoad() {
  }

  forgotPassword(): void {
    this.showForgotPassword = true;
  }

  onSubmit(form: any): void {
    this.auth.signIn({ email: form.email, password: form.password })
      .then(data => {
        if (data.auth.emailVerified) {
          this._authenticated = true;
          this.user$.getByEmail(form.email).subscribe(users => {
            let state = {
              user: users[0]
            };
            this.state$.setState(state);
            this.navCtrl.setRoot(HomePage);
          })
        } else {
          this.auth.userInfo().subscribe(user => { this.email = user.email; });
          this.showVerification = true;
        }
      })
      .catch(err => {
        console.error(err);
        this.errorMessage = err.message;
      });
  }

  onViewCanLeave(): boolean {
    // here we can either return true or false
    // depending on if we want to leave this view
    return this._authenticated;
  }

  resetPassword(form: any) {
    this.auth.resetPassword(form.controls.email.value)
      .then(() => {
        let toast = this.toastCtrl.create({ message: 'Password reset email sent.', duration: 4000 });
        toast.present();
      })
      .catch(err => {
        let toast = this.toastCtrl.create({ message: err.message, duration: 4000 });
        toast.present();
      });
  }

  sendVerificationEmail(): void {
    this.auth.sendVerificationEmail();
  }
}
