import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { DeviceDetailsPage } from '../device-details/device-details';
import { SearchService } from '../../providers/search-service';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {
  query: string;
  devices: any;
  constructor(public navCtrl: NavController, public search$: SearchService) { }

  ionViewDidLoad() {

  }

  clear() {
    this.query = '';
    this.devices = [];
  }

  keypress($event) {
    if ($event.charCode === 13) {
      this.search();
    }
  }

  search() {
    if (this.query.length > 0) {
      this.search$.all.subscribe(
        data => {
          this.devices = data.filter(value => {
            return !this.query || value.location.toLowerCase().indexOf(this.query.toLowerCase()) !== -1;
          });
        }
      );
    }
  }

  showDetails(key: string) {
    this.navCtrl.push(DeviceDetailsPage, { key: key })
  }

  getClass(deviceName: any) {
    return 'icon-' + deviceName[0].toLowerCase();
  }
}
