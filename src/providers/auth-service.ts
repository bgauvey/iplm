import { Injectable } from '@angular/core';
import { AngularFire, AngularFireAuth, FirebaseAuthState } from 'angularfire2';
import { UserProfileService } from './user-profile-service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

// import * as firebase from 'firebase';

export class UserInfo {
  last_name: string;
  first_name: string;
  displayName: string;
  email: string;
  emailVerified: boolean;
  photoURL: string;
  uid: string;
  isAdmin: boolean;
  $key?: string;
  constructor(displayName: string, email: string, emailVerified: boolean, photoURL: string, uid: string,
    last_name: string, first_name: string, isAdmin: boolean, $key: string) {
    this.displayName = displayName;
    this.email = email;
    this.emailVerified = emailVerified;
    this.photoURL = photoURL;
    this.uid = uid;
    this.last_name = last_name;
    this.first_name = first_name;
    this.isAdmin = isAdmin;
    this.$key = $key;
  }
}

@Injectable()
export class AuthService {
  authState: FirebaseAuthState = null;
  auth: AngularFireAuth = null;

  constructor(private af: AngularFire, private user$: UserProfileService) {
    this.auth = this.af.auth;
    this.auth.subscribe(auth => {
      this.authState = auth;
    });
  }

  get userImage(): string {
    let path: string;
    this.af.auth.subscribe((data) => {
      if (data) {
        path = data.auth.photoURL;
      }
    });
    return path;
  }

  userInfo(): Observable<UserInfo> {
    let userSubject: BehaviorSubject<UserInfo> = new BehaviorSubject(new UserInfo('', '', false, '', '', '', '', false, null));
    let userInfo: UserInfo;
    this.user$.getByEmail(this.authState.auth.email).subscribe(
      user => {
        if (user[0]) {
          userInfo = new UserInfo(
            this.authState.auth.displayName,
            this.authState.auth.email,
            this.authState.auth.emailVerified,
            this.authState.auth.photoURL,
            this.authState.auth.uid,
            user[0].last_name,
            user[0].first_name,
            user[0].isAdmin,
            user[0].$key);
          userSubject.next(userInfo);
        }
      },
      err => {
        console.error(err.message);
      });
    return userSubject.asObservable();
  }

  get authenticated(): boolean {
    return this.authState !== null;
  }

  get id(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  signIn(credentials: any): firebase.Promise<FirebaseAuthState> {
    return this.auth.login(credentials);
  }

  signOut(): void {
    this.auth.logout();
  }

  sendVerificationEmail(): void {
    this.authState.auth.sendEmailVerification();
  }

  resetPassword(email: string): firebase.Promise<any> {
    return firebase.auth().sendPasswordResetEmail(email);
  }

  changePassword(newPassword: string): firebase.Promise<any> {
    return this.auth.getAuth().auth.updatePassword(newPassword);
  }
}
