import { Injectable } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { Observable } from 'rxjs/Observable';
import { IDevice } from '../models/device-model';


@Injectable()
export class DeviceService {
  private devicesRef: FirebaseListObservable<IDevice[]>;
  private path: string;

  constructor(private af: AngularFire) {
    this.path = `/devices`;

    this.devicesRef = af.database.list(this.path);
  }

  get(): FirebaseListObservable<IDevice[]> {
    return this.devicesRef;
  }

  getByParentKey(key: string): FirebaseListObservable<IDevice[]> {
    return this.af.database.list(this.path, {
      query: {
        orderByChild: 'group_key',
        equalTo: key
      }
    });
  }

  getById(id: string): any {
    let device: FirebaseObjectObservable<IDevice>;
    device = this.af.database.object(this.path + `/` + id);
    return Observable.merge(device);
  }

  create(device: any): firebase.Promise<any> {
    return this.devicesRef.push(device);
  }

  remove(key: string): firebase.Promise<any> {
    return this.devicesRef.remove(key);
  }

  update(key: string, changes: any): firebase.Promise<any> {
    return this.devicesRef.update(key, changes);
  }
}
