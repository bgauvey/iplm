import { Injectable } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { Observable } from 'rxjs/Observable';

import { IGroup, Group } from '../models//group-model';


@Injectable()
export class GroupService {
  private groupsRef: FirebaseListObservable<IGroup[]>;
  private path: string;

  constructor(private af: AngularFire) {
    this.path = `/groups`;

    this.groupsRef = af.database.list(this.path);
  }

  get(): FirebaseListObservable<IGroup[]> {
    return this.groupsRef;
  }

  getById(id: string): any {
    let group: FirebaseObjectObservable<IGroup>;
    group = this.af.database.object(this.path + `/` + id);
    return Observable.merge(group);
  }

  create(group: IGroup): firebase.Promise<any> {
    return this.groupsRef.push(new Group(
      group.name,
      group.notes,
      group.ip_info
    ));
  }

  remove(group: IGroup): firebase.Promise<any> {
    return this.groupsRef.remove(group.$key);
  }

  update(group: IGroup, changes: any): firebase.Promise<any> {
    return this.groupsRef.update(group.$key, changes);
  }
}
