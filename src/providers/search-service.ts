import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/switchMap';

import { Injectable } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Injectable()
export class SearchService {
  public all: FirebaseListObservable<any[]>;
  private path: string;

  constructor(private af: AngularFire) {
    this.path = `/devices`;

    this.all = af.database.list(this.path);
  }
}
