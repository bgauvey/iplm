import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
/*
  Generated class for the StateService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
let state = {};

@Injectable()
export class StateService {
    state: BehaviorSubject<any> = new BehaviorSubject(state);

    constructor() {
    }

    getState() {
        let obj = JSON.parse(localStorage.getItem('state'));
        this.state = new BehaviorSubject(obj);
        return this.state.asObservable();
    }

    setState(newState: any): void {
        localStorage.setItem('state', JSON.stringify(newState));
        this.state.next(Object.assign({}, newState));
    }

    removeState(): void {
        localStorage.removeItem('state');
    }
}
