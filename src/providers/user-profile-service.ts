import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/switchMap';

import { Injectable } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserProfileService {
  private $ref: FirebaseListObservable<IUser[]>;
  private path: string;

  constructor(private af: AngularFire) {
    this.path = `/users`;

    this.$ref = af.database.list(this.path);
  }

  get(): FirebaseListObservable<IUser[]> {
    return this.$ref;
  }

  getByEmail(email: string): FirebaseListObservable<IUser[]> {
    return this.af.database.list(this.path, {
      query: {
        orderByChild: 'email',
        equalTo: email,
        limit: 1
      }
    });
  }

  getById(id: string): Observable<IUser> {
    let user: FirebaseObjectObservable<IUser>;
    user = this.af.database.object(this.path + `/` + id);
    return Observable.merge(user);
  }

  create(user: any): firebase.Promise<any> {
    return this.$ref.push(user);
  }

  remove(key: string): firebase.Promise<any> {
    return this.$ref.remove(key);
  }

  update(key: string, changes: any): firebase.Promise<any> {
    return this.$ref.update(key, changes);
  }
}

export interface IUser {
  $key?: string;
  email: string;
  first_name: string;
  last_name: string;
  isAdmin: boolean;
}

export class User implements IUser {
  email: string;
  first_name: string;
  last_name: string;
  isAdmin: boolean;
  $key: string;

  constructor(email: string, last_name: string, first_name: string, isAdmin: boolean, $key?: any) {
    this.email = email;
    this.last_name = last_name;
    this.first_name = first_name;
    this.isAdmin = isAdmin;
  }
}
